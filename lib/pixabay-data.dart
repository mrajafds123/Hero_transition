class ImageData {
  int id;
  String title;
  String author;
  String imageLarge;

  ImageData({this.id, this.title, this.author, this.imageLarge});

  static List<ImageData> pixabay;

  ImageData.init() {
    pixabay = List();
    pixabay.add(
      ImageData(id: 1, title: 'Kamera Nikon', author: 'Cocok untuk Foto', imageLarge: 'https://cdn.pixabay.com/photo/2018/10/11/09/24/slr-camera-3739242_960_720.jpg'),
    );
    pixabay.add(
      ImageData(id: 2, title: 'Kamera Canon', author: 'Cocok untuk Vlog & Foto', imageLarge: 'https://cdn.pixabay.com/photo/2019/11/09/14/28/camera-4613669_960_720.jpg'),
    );
    pixabay.add(
      ImageData(id: 3, title: 'Kamera Sony', author: 'Cocok untuk Film & Foto', imageLarge: 'https://cdn.pixabay.com/photo/2019/12/01/10/44/sony-alpha-7-iii-4665165_960_720.jpg'),
    );
    pixabay.add(
      ImageData(id: 4, title: 'Kamera Fuji', author: 'Cocok untuk Vlog & Foto', imageLarge: 'https://cdn.pixabay.com/photo/2014/12/13/10/24/camera-566421_960_720.jpg'),
    );
    pixabay.add(
      ImageData(id: 5, title: 'Kamera Olympus', author: 'Cocok untuk Foto', imageLarge: 'https://cdn.pixabay.com/photo/2019/10/27/04/00/olympus-4580642_960_720.jpg'),
    );
  }
}
